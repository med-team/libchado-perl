#!/usr/bin/perl -w
use strict;

use DBI;
use Bio::GMOD::Config;
use Bio::GMOD::DB::Config;
use Getopt::Long;
use URI::Escape;



my $gmod_conf = $ENV{'GMOD_ROOT'} ?
                Bio::GMOD::Config->new($ENV{'GMOD_ROOT'}) :
                Bio::GMOD::Config->new();

my $db_conf = Bio::GMOD::DB::Config->new($gmod_conf,'gmod-chado');

my $dbh = eval {$db_conf->dbh};
if( $@ =~ m/couldn't create db connection/i ) {
 exit 102;
}

my $nbtables = $dbh->prepare("select count(*) as nbtables from pg_tables");
$nbtables->execute() or exit 102;
my $arrayref = $nbtables->fetchrow_arrayref();
$nbtables   = $$arrayref[0];
$dbh->disconnect;

if($nbtables>0) {
   print "Database already exists, update it\n";
  exit 101;
}
else {
  print "Empty database, create it\n";
  exit 100;
}

